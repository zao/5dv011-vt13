extern crate palette;
use palette::{Rgb, Hsv, Gradient};
use palette::pixel::{GammaRgb, RgbPixel};

use std::ops::Add;
use std::env;
use std::fs;
use std::io::Write;

#[derive(Debug, Clone, Copy)]
struct Complex {
    x: f32,
    y: f32,
}

impl Complex {
    fn new(x: f32, y: f32) -> Complex {
        Complex { x: x, y: y }
    }
}

impl Add for Complex {
    type Output = Complex;
    fn add(self, other: Complex) -> Complex {
        Complex {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

fn square(z: Complex) -> Complex {
    Complex {
        x: z.x * z.x - z.y * z.y,
        y: 2.0 * z.x * z.y,
    }
}

fn abs(z: Complex) -> f32 {
    (z.x * z.x + z.y * z.y).sqrt()
}

fn mandelbrot_inside(c: Complex, max_iter: u32) -> bool {
    let mut z = Complex::new(0.0, 0.0);
    for _ in 1..max_iter + 1 {
        z = square(z) + c;
        if abs(z) > 2.0 {
            return false;
        }
    }
    true
}

fn mandelbrot_iters(c: Complex, max_iter: u32) -> u32 {
    let mut z = Complex::new(0.0, 0.0);
    for k in 1..max_iter + 1 {
        z = square(z) + c;
        if abs(z) > 2.0 {
            return k;
        }
    }
    max_iter
}

struct Domain {
    width: u32,
    height: u32,
    pixel_size: f32,
    center: Complex,
}

impl Domain {
    fn new(width: u32, height: u32, pixel_size: f32, center: Complex) -> Domain {
        Domain {
            width: width,
            height: height,
            pixel_size: pixel_size,
            center: center,
        }
    }

    fn coord(&self, x: u32, y: u32) -> Complex {
        let fx = x as f32;
        let fy = y as f32;
        let mid_x = self.width as f32 / 2.0;
        let mid_y = self.height as f32 / 2.0;
        let x_amt = fx - mid_x;
        let y_amt = fy - mid_y;
        Complex {
            x: self.center.x + x_amt * self.pixel_size,
            y: self.center.y + y_amt * self.pixel_size,
        }
    }
}

fn write_8bit_pnm(filename: String, width: u32, height: u32, pixels: &[u8]) {
    let mut f = fs::File::create(filename).unwrap();
    write!(f, "P6 {} {} 255\n", width, height);
    f.write_all(pixels);
}

fn map_intensity(value: u32, max_value: u32) -> Rgb {
    let grad = Gradient::new(vec![
        Hsv::from(Rgb::new(1.0, 0.1, 0.1)),
        Hsv::from(Rgb::new(0.1, 1.0, 1.0))
    ]);
    let frac = value as f64 / max_value as f64;
    let c = Rgb::from(grad.get(frac as f32));
    c
}

fn main() {
    let args: Vec<_> = env::args().collect();
    let width: u32 = args[1].parse().unwrap();
    let height: u32 = args[2].parse().unwrap();
    let pixel_size: f32 = args[3].parse().unwrap();
    let center = Complex::new(args[4].parse().unwrap(), args[5].parse().unwrap());
    let max_iter: u32 = args[6].parse().unwrap();
    let out_filename = args[7].clone();
    let domain = Domain::new(width, height, pixel_size, center);
    let mut field: Vec<u8> = Vec::with_capacity((width * height * 3) as usize);
    for y in 0..height {
        for x in 0..width {
            let c = domain.coord(x, y);
            let inside = mandelbrot_inside(c, max_iter);
            let iters = mandelbrot_iters(c, max_iter);
            let color: Rgb = map_intensity(iters, max_iter);
            field.push((255.0 * color.red) as u8);
            field.push((255.0 * color.green) as u8);
            field.push((255.0 * color.blue) as u8);
        }
    }
    write_8bit_pnm(out_filename, width, height, field.as_slice());
}
